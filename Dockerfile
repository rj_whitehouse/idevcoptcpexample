# Stage 1 - the build process
FROM node:latest as build-idevcoptcpexample
WORKDIR /usr/src/app
COPY package*.json ./
RUN npm install
COPY . ./
RUN npm run-script build


# Stage 2 - the production environment
FROM nginx:1.12-alpine
# COPY default.conf /etc/nginx/conf.d/default.conf
COPY --from=build-idevcoptcpexample /usr/src/app/build /usr/share/nginx/html
# EXPOSE 3007
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]