import React, { Component } from 'react'

class PlayerListDetails extends Component {
    render() {
      var className = "";
      if(this.props.player.gameCompleted) {
        className = "completed";
      }
      console.log(this.props);
      return (<div key = {this.props.keyName} className={className}>{this.props.player.name}</div>)
    }
  }

export default PlayerListDetails