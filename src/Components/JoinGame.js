import React, { Component } from 'react'
import PubSub from 'pubsub-js';

class JoinGame extends Component {

  render() {
    function handleClick(e) {
      e.preventDefault();
      PubSub.publish('ActionToTake', "JoinClicked");
    }

    return (
      <div className="joinGame"><input type="button" name="joinGame" value="join" onClick={handleClick} /></div>
    );
  }
}

export default JoinGame