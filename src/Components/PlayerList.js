import React, { Component } from 'react'
import PlayerListDetails from './PlayerListDetails'

class PlayerList extends Component {

  render() {
    const playerList = this.props.playerList;

    return(
      playerList.map((gh, i)=>(
        <PlayerListDetails key = {"playerListDetails" + i} keyName = {"playerListDetails" + i} player={gh} />
      )));
  }
}

export default PlayerList