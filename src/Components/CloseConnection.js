import React, { Component } from 'react'
import PubSub from 'pubsub-js';

class CloseConnection extends Component {

  render() {
    function handleClick(e) {
      e.preventDefault();
      PubSub.publish('CloseConnection', "Close");
    }

    return (
      <div className="closeConnection"><input type="button" name="closeConnection" value="Close Connection" onClick={handleClick} /></div>
    );
  }
}

export default CloseConnection