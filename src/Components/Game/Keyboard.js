import React from 'react';
import KeyboardLetter from './KeyboardLetter'
import './Keyboard.css'

const Keyboard = () => (
	<div className="keyboard">
        <KeyboardLetter data = {"A"} />
		<KeyboardLetter data = {"B"} />
		<KeyboardLetter data = {"C"} />
		<KeyboardLetter data = {"D"} />
        <KeyboardLetter data = {"E"} />
		<KeyboardLetter data = {"F"} />
		<KeyboardLetter data = {"G"} />
		<KeyboardLetter data = {"H"} />
        <KeyboardLetter data = {"I"} />
		<KeyboardLetter data = {"J"} />
		<KeyboardLetter data = {"K"} />
		<KeyboardLetter data = {"L"} />
        <KeyboardLetter data = {"M"} />
		<KeyboardLetter data = {"N"} />
		<KeyboardLetter data = {"O"} />
		<KeyboardLetter data = {"P"} />
        <KeyboardLetter data = {"Q"} />
		<KeyboardLetter data = {"R"} />
        <KeyboardLetter data = {"S"} />
		<KeyboardLetter data = {"T"} />
		<KeyboardLetter data = {"U"} />
		<KeyboardLetter data = {"V"} />
        <KeyboardLetter data = {"W"} />
		<KeyboardLetter data = {"X"} />
		<KeyboardLetter data = {"Y"} />
		<KeyboardLetter data = {"Z"} />
	</div>
)

export default Keyboard