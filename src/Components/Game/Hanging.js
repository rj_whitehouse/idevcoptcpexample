import React, { Component } from 'react';
import './Hanging.css'

class Hanging extends Component {
	render() {
		const incorrectGuesses = this.props.incorrectGuesses;
		return(
			<div className="hanging">{incorrectGuesses} incorrect guesses</div>
		);
	}
}
  
export default Hanging