import React, { Component } from 'react'
import './KeyboardLetter.css'
import PubSub from 'pubsub-js';

class KeyboardLetter extends Component {

    handleClick = () => {
        PubSub.publish("LetterClicked", this.props.data);
    }

    render() {
        return (<div className="keyboardLetter" onClick={this.handleClick}>{this.props.data}</div>)
    }
}
  
export default KeyboardLetter