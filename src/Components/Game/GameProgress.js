import React, { Component } from 'react'
import './GameProgress.css'

class GameProgress extends Component {

  render() {
    const guessPhrase = this.props.guessPhrase;
    return(
      <div className="gameBoard">{guessPhrase}</div>
    );
  }
}

export default GameProgress