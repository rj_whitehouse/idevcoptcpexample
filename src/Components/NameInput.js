import React, { Component } from 'react'
import PubSub from 'pubsub-js';

class NameInput extends Component {

  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(event) {
    PubSub.publish('NameUpdated', event.target.value);
  }

  render() {
    const currentName = this.props.name;
    return(
      <div className="nameInput">Your Name: <input type="text" name="nameInput" value={currentName} onChange={this.handleChange} /></div>
    );
  }
}

export default NameInput