import React, { Component } from 'react'
import NameInput from './Components/NameInput'
import JoinGame from './Components/JoinGame'
import GameProgress from './Components/Game/GameProgress'
import Keyboard from './Components/Game/Keyboard'
import Hanging from './Components/Game/Hanging'
import PlayerList from './Components/PlayerList'
import PubSub from 'pubsub-js';

// one machine
// const URL = 'ws://127.0.0.1:3500'

// same network
// const URL = 'ws://192.168.1.110:3500'

// london aws
const URL = 'ws://52.56.68.91:80'

class Game extends Component {
  state = {
    name: '',
    status: 'disconnected',
    playerKey: 0,
    nameAccepted: false,
    guessPhrase: "",
    incorrectGuesses: 0,
    playerList: [],
    gameCompleted: false
  }

  ws = new WebSocket(URL);

  componentDidMount() {
    this.ws.onopen = () => {
      // on connecting, do nothing but log it to the console
      console.log('connected')
      console.log(this);
      this.setState({
        status: 'connected'
      })
      //this.sendCommandSubscriber("","PlayerStatus");
    }

    this.ws.onmessage = evt => {
      // console.log("received message!");
      // console.log(evt.data);
      // on receiving a message, add it to the list of messages
      const message = JSON.parse(evt.data);
      console.log("Message Received!");
      console.log(message);
      // console.log(message.name);
      if (message.name === "PlayerData") {
        if (this.state.playerKey !== 0 && message.player.PlayerKey !== 0) {
          this.setState({ 
            guessPhrase: message.player.currentGameProgress, 
            incorrectGuesses: message.player.incorrectGuesses, 
            gameCompleted: message.player.gameCompleted 
          });
        }
        else if (message.player.PlayerKey !== 0) {
          console.log("name accepted!");
          this.setState({nameAccepted: true, playerKey: message.player.PlayerKey, guessPhrase: message.player.currentGameProgress });
        } else {
          console.log("name already taken or rejected!");
          this.setState({name: '' });
        }
        
      }
      else if(message.name === "Ping") {
        this.submitMessage("Pong", "Response");
      } else if(message.name == "PlayerListData") {
        console.log("received updated player list!");
        this.setState({playerList: message.playerList });
      } else {
        this.addMessage(message)
      }
    }

    this.ws.onclose = () => {
      console.log('disconnected2');
      this.setState({
        status: 'disconnected'
      })
      // automatically try to reconnect on connection loss
      this.setState({
        ws: new WebSocket(URL),
      })
    }

    this.ws.onerror = evt => {
      console.log("an error has occured!");
    }

    this.token = PubSub.subscribe('SendCommand', this.sendCommandSubscriber.bind(this));
    this.token = PubSub.subscribe('ActionToTake', this.actionToTake.bind(this));
    this.token = PubSub.subscribe('NameUpdated', this.nameUpdated.bind(this));
    this.token = PubSub.subscribe('LetterClicked', this.letterClicked.bind(this));
  }

  nameUpdated (msg, command){
    console.log("I've received a name update I should update! " + command);
    this.setState({
      name: command
    })
  }

  letterClicked (msg, command){
    this.submitMessage("NewGuess", command);
  }

  actionToTake (msg, command){
    console.log("I've received a action I should make! " + command);
    // const commandMessage = { action: command };
    if (command === "JoinClicked") {
      if (this.state.name === "") {
        console.log("oi! you need to put a name in!");
      } else {
        console.log("asking server if " + this.state.name + " is ok as a name");
        this.submitMessage("Joining", this.state.name);
      }
    }
  }

  sendCommandSubscriber (msg, command){
    console.log("I've received a command I should send! " + command);
    const commandMessage = { message: msg, command: command  };
    this.sendMessage(commandMessage);
  }

  addMessage = message =>
    this.setState(state => ({ messages: [message, ...state.messages] }))

  submitMessage (messageString, command) {
    console.log("message sending!");
    const message = { playerKey: this.state.playerKey, message: messageString, command: command }
    console.log(message);
    this.ws.send(JSON.stringify(message))
    // console.log(this.ws);
    //this.addMessage(message)
  }

  sendMessage = messageString => {
    console.log(messageString);
    this.ws.send(JSON.stringify(messageString));
  }

  submitCommand = commandString => {
    const command = { command: this.state.name, message: commandString }
    console.log(command);
    this.ws.send(JSON.stringify(command))
    console.log(this.ws);
    this.addMessage(command)
  }

  render() {
    // console.log(this.state.isPlaying + " is the playing state");
    var outputData = '';

    if (this.state.status === 'disconnected') {
      outputData = (
        <div>Attemping Connection...</div>
      );
    }
    else if (this.state.playerKey === 0) {
      outputData = (
        <div>
          <NameInput name={this.state.name} />
          <JoinGame />
        </div>
      );
    } else if(this.state.gameCompleted === true) {
      outputData = (
        <div>Congratulations! Game Completed. Awaiting Next Game.</div>
      )
    } else if (this.state.guessPhrase === "" || this.state.guessPhrase == null || this.state.gameCompleted == true) {
      outputData = (
        <div>Awaiting Game...</div>
      )
    }
    else {
      outputData = (
        <div>
          <GameProgress guessPhrase={this.state.guessPhrase} />
          <Keyboard />
          <Hanging incorrectGuesses={this.state.incorrectGuesses} />
        </div>);
    }

    return (
      <div>
        <div className="statusMessage">{this.state.status}</div>
        <div className="playerList"><PlayerList playerList={this.state.playerList} /></div>
        {outputData}
      </div>
    )
  }
}

export default Game